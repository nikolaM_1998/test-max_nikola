FROM java:8-jdk-alpine
COPY ./target/test_project-1.0-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "test_project-1.0-SNAPSHOT.jar"]
